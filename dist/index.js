"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var cors_1 = __importDefault(require("cors"));
var datastore_1 = require("@google-cloud/datastore");
var app = (0, express_1["default"])();
app.use(express_1["default"].json());
app.use((0, cors_1["default"])());
var datastore = new datastore_1.Datastore();
/**
 * Check for valid email. If valid then check password.
 * If password is valid then respond with fname and lname.
 */
app.patch("/users/login", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var query, response, email, password, i;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                query = datastore.createQuery("Employee");
                return [4 /*yield*/, datastore.runQuery(query)];
            case 1:
                response = _a.sent();
                email = req.body["email"];
                password = req.body["password"];
                for (i = 0; i < response[0].length; i++) {
                    if (email === response[0][i]["email"]) {
                        if (password === response[0][i]["password"]) {
                            res.status(200).send({ fname: response[0][i]["fname"], lname: response[0][i]["lname"] });
                            return [2 /*return*/];
                        }
                    }
                }
                res.status(404).send(false);
                return [2 /*return*/];
        }
    });
}); });
app.get("/users/:email/verify", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var key, employee;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                key = datastore.key(["Employee", req.params.email]);
                return [4 /*yield*/, datastore.get(key)];
            case 1:
                employee = _a.sent();
                if (employee[0]) {
                    res.status(200).send(true);
                }
                else {
                    res.status(404).send(false);
                }
                return [2 /*return*/];
        }
    });
}); });
// get all employees
app.get("/users", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var query, response, emails;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                query = datastore.createQuery("Employee");
                return [4 /*yield*/, datastore.runQuery(query)];
            case 1:
                response = _a.sent();
                emails = response[0].map(function (e) { return e.email; });
                res.status(200).send(emails);
                return [2 /*return*/];
        }
    });
}); });
var PORT = process.env.PORT || 3002;
app.listen(PORT, function () { console.log("Application started"); });
